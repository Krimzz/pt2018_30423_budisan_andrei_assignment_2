package edu.ro.utcn.Market.GUI;

import com.jfoenix.controls.*;
import edu.ro.utcn.Market.Cashier.Client;
import edu.ro.utcn.Market.Scheduler.Scheduler;
import edu.ro.utcn.Market.Scheduler.SelectionPolicy;
import edu.ro.utcn.Market.Simulation.SimulationManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;

import java.util.concurrent.BlockingQueue;

public class MainController {
    @FXML
    private JFXButton start;

    @FXML
    private JFXButton reset;

    @FXML
    private JFXTextField arrTimeMin;

    @FXML
    private JFXTextField arrTimeMax;

    @FXML
    private JFXTextField servTimeMin;

    @FXML
    private JFXTextField servTimeMax;

    @FXML
    private JFXTextField simulationTime;

    @FXML
    private JFXTextField nrClients;

    @FXML
    private JFXTextField nrCashRegisters;

    @FXML
    private JFXTextField nrClientsPerCash;

    @FXML
    private JFXListView<Label> cashreg1;

    @FXML
    private JFXListView<Label> cashreg2;

    @FXML
    private JFXListView<Label> cashreg3;

    @FXML
    private JFXSpinner loading;

    @FXML
    private JFXToggleButton policy;

    @FXML
    private JFXTextArea log;

    @FXML
    private Tab logTab;

    @FXML
    public void handleButtonAction(MouseEvent event) {
        if (event.getSource() == start && verifyInput()) {

            logTab.setContent(log);
            setLoading(true);

            SimulationManager gen = new SimulationManager(getSimulationTime(), getServTimeMax(), getServTimeMin(),
                    getArrTimeMax(), getArrTimeMin(), getnrCashRegisters(), getnrClients(), getnrClientsPerCash(), getPolicy(), this);
            Thread t = new Thread(gen);
            t.setDaemon(true);
            t.start();
        }

        if (event.getSource() == reset) {
            //stop threads???
            Scheduler.stopCashRegisters();

            setLoading(false);
            cashreg1.getItems().clear();
            cashreg2.getItems().clear();
            cashreg3.getItems().clear();
            log.setText("");
        }

        if (event.getSource() == policy) {
            if (policy.isSelected()) {
                policy.setText("Shortest Time");
            } else {
                policy.setText("Shortest Queue");
            }
        }
    }

    public int getSimulationTime() {
        int val = -1;
        try {
            val = Integer.parseInt(simulationTime.getText());
            simulationTime.getStyleClass().removeIf(style -> style.equals("invalid-input"));
        }catch (NumberFormatException e){
            simulationTime.getStyleClass().add("invalid-input");
        }
        return val;
    }

    public int getArrTimeMin() {
        int val = -1;
        try {
            val = Integer.parseInt(arrTimeMin.getText());
            arrTimeMin.getStyleClass().removeIf(style -> style.equals("invalid-input"));
        }catch (NumberFormatException e){
            arrTimeMin.getStyleClass().add("invalid-input");
        }
        return val;
    }

    public int getArrTimeMax() {
        int val = -1;
        try {
            val = Integer.parseInt(arrTimeMax.getText());
            arrTimeMax.getStyleClass().removeIf(style -> style.equals("invalid-input"));
        }catch (NumberFormatException e){
            arrTimeMax.getStyleClass().add("invalid-input");
        }
        return val;
    }

    public int getServTimeMin() {
        int val = -1;
        try {
            val = Integer.parseInt(servTimeMin.getText());
            servTimeMin.getStyleClass().removeIf(style -> style.equals("invalid-input"));
        }catch (NumberFormatException e){
            servTimeMin.getStyleClass().add("invalid-input");
        }
        return val;
    }

    public int getServTimeMax() {
        int val = -1;
        try {
            val = Integer.parseInt(servTimeMax.getText());
            servTimeMax.getStyleClass().removeIf(style -> style.equals("invalid-input"));
        }catch (NumberFormatException e){
            servTimeMax.getStyleClass().add("invalid-input");
        }
        return val;
    }

    public int getnrClients() {
        int val = -1;
        try {
            val = Integer.parseInt(nrClients.getText());
            nrClients.getStyleClass().removeIf(style -> style.equals("invalid-input"));
        }catch (NumberFormatException e){
            nrClients.getStyleClass().add("invalid-input");
        }
        return val;
    }

    public int getnrCashRegisters() {
        int val = -1;
        try {
            val = Integer.parseInt(nrCashRegisters.getText());
            nrCashRegisters.getStyleClass().removeIf(style -> style.equals("invalid-input"));
        }catch (NumberFormatException e){
            nrCashRegisters.getStyleClass().add("invalid-input");
        }
        return val;
    }

    public int getnrClientsPerCash() {
        int val = -1;
        try {
            nrClientsPerCash.getStyleClass().removeIf(style -> style.equals("invalid-input"));
            val = Integer.parseInt(nrClientsPerCash.getText());
        }catch (NumberFormatException e){
            nrClientsPerCash.getStyleClass().add("invalid-input");
        }
        return val;
    }

    public SelectionPolicy getPolicy() {
        if (policy.isSelected()) {
            return SelectionPolicy.SHORTEST_TIME;
        } else {
            return SelectionPolicy.SHORTEST_QUEUE;
        }
    }

    public void showListView(int number, BlockingQueue<Client> clients) {
        Platform.runLater(new Runnable() {
            public void run() {
                if (number == 1) {
                    cashreg1.getItems().clear();
                    for (Client client : clients) {
                        cashreg1.getItems().add(new Label("Client " + client.getNumber()));
                    }
                }
                if (number == 2) {
                    cashreg2.getItems().clear();
                    for (Client client : clients) {
                        cashreg2.getItems().add(new Label("Client " + client.getNumber()));
                    }
                }
                if (number == 3) {
                    cashreg3.getItems().clear();
                    for (Client client : clients) {
                        cashreg3.getItems().add(new Label("Client " + client.getNumber()));
                    }
                }
            }
        });
    }

    public synchronized void outLog(final String string){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                log.appendText("  " + string);
            }
        });
    }

    public boolean verifyInput() {
        boolean ok = true;
        if (getSimulationTime() == -1) {
            ok = false;
        }

        if (getArrTimeMin() == -1) {
            ok = false;
        }

        if (getArrTimeMax() == -1) {
            ok = false;
        }

        if (getServTimeMin() == -1) {
            ok = false;
        }

        if (getServTimeMax() == -1) {
            ok = false;
        }

        if (getnrClients() == -1) {
            ok = false;
        }

        if (getnrCashRegisters() == -1) {
            ok = false;
        }

        if (getnrClientsPerCash() == -1) {
            ok = false;
        }

        return ok;
    }

    public void setLoading(boolean state){
        loading.setVisible(state);
    }
}
