package edu.ro.utcn.Market.Cashier;

import java.util.concurrent.atomic.AtomicInteger;

public class Client {
    private int number;
    private AtomicInteger arrivalTime;
    private AtomicInteger processTime;
    private AtomicInteger finishTime; //finishTime = arrivalTime + processTime+ waitingPeriodOnChosenServer

    public AtomicInteger getArrivalTime() {
        return arrivalTime;
    }

    public AtomicInteger getProcessTime() {
        return processTime;
    }

    public void setFinishTime(int waittingTime) {  //waiting time de la queue
        this.finishTime.set(arrivalTime.get() + processTime.get() + waittingTime);
    }

    public Client(int number, int arrival, int process) {
        this.arrivalTime = new AtomicInteger();
        this.processTime = new AtomicInteger();
        this.finishTime = new AtomicInteger();
        this.number = number;

        arrivalTime.set(arrival);
        processTime.set(process);
    }

    @Override
    public String toString() {
        return "Client " + number +
                " arrT=" + arrivalTime +
                " srvT=" + processTime +
                " finT=" + finishTime;
    }

    public int getNumber(){
        return number;
    }
}
