package edu.ro.utcn.Market.Cashier;

import edu.ro.utcn.Market.GUI.MainController;
import edu.ro.utcn.Market.Simulation.SimulationManager;
import sun.applet.Main;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class CashRegister implements Runnable {
    private BlockingQueue<Client> clients;
    private AtomicInteger waitTime;
    private volatile boolean isRunning;
    private int number;

    private int avWaitTime;
    private int avProcessTime;
    private int nrClients;
    private int emptyQTime;

    private MainController gui;

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public CashRegister(int maxCapacity, int number,MainController gui) {
        //init queue and wait time
        this.gui = gui;

        System.out.println("CashRegister " + number + " : opened ");
        gui.outLog("CashRegister " + number + ": opened " + "\n");

        clients = new ArrayBlockingQueue<>(maxCapacity);//nr max de elem care pot fi la CashRegister
        waitTime = new AtomicInteger();
        waitTime.set(0);
        isRunning = true;
        this.number = number;

        avWaitTime = 0;
        avProcessTime = 0;
        nrClients = 0;
        emptyQTime = 0;
    }

    public void addClient(Client newClient) {
        //add client to queue
        //increment waitTime
        try {

            clients.put(newClient);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        newClient.setFinishTime(this.getWaitTime().get());
        waitTime.set(waitTime.get() + newClient.getProcessTime().get());
        avWaitTime += waitTime.get();
        avProcessTime += newClient.getProcessTime().get();
        nrClients++;

        System.out.println(newClient.toString() + " JOINED " + this.toString());
        gui.outLog(newClient.toString() + " JOINED " + this.toString() + "\n");
        gui.showListView(number,clients);
    }

    public int getLength() {
        return clients.size();
    }

    public AtomicInteger getWaitTime() {
        return waitTime;
    }

    @Override
    public String toString() {
        return "CashRegister" +
                " number= " + number +
                " waitTime= " + waitTime +
                " NrClients= " + clients.size();
    }

    public int getAvWaitTime() {
        return this.avWaitTime /this.nrClients;
    }

    public int getAvProcessTime() {
        return this.avProcessTime / this.nrClients;
    }

    public int getEmptyQTime() {
        return this.emptyQTime;
    }

    public void incrementEmptyQueueTime() {
        if (this.clients.size() == 0) {
            this.emptyQTime++;
        }
    }

    @Override
    public void run() {
        while (isRunning) {
            //take next client from queue
            //stop thread for a time equal with client's processing time
            //decrement waitTime
            try {

                Client client = clients.peek();
                if (client != null) {
                    int processingTime = client.getProcessTime().get();
                    while (processingTime > 0) {
                        processingTime--;
                        Thread.sleep(1000);
                        waitTime.set(waitTime.get() - 1);
                    }
                    clients.take();

                    System.out.println(client.toString() + " LEFT " + this.toString());
                    gui.outLog(client.toString() + " LEFT " + this.toString() + "\n");
                    gui.showListView(number,clients);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
