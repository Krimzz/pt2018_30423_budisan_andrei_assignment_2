package edu.ro.utcn.Market.Scheduler;

import edu.ro.utcn.Market.Cashier.CashRegister;
import edu.ro.utcn.Market.Cashier.Client;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcreteStrategyTime implements Strategy {

    @Override
    public void addClient(ArrayList<CashRegister> cashRegisters, Client client) {
        AtomicInteger minQueueWaitingTime = cashRegisters.get(0).getWaitTime();
        int shortestLength = cashRegisters.get(0).getLength();
        CashRegister found = cashRegisters.get(0);

        for (CashRegister cashRegister : cashRegisters) {
            if (minQueueWaitingTime.get() > cashRegister.getWaitTime().get()) {

                minQueueWaitingTime = cashRegister.getWaitTime();
                found = cashRegister;
                shortestLength = cashRegister.getLength();

            } else {
                if (minQueueWaitingTime.get() == cashRegister.getWaitTime().get() && shortestLength > cashRegister.getLength()) {

                    minQueueWaitingTime = cashRegister.getWaitTime();
                    found = cashRegister;
                    shortestLength = cashRegister.getLength();

                }
            }
        }

        found.addClient(client);
    }
}
