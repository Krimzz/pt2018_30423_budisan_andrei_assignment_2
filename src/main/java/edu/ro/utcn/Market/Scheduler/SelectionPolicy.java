package edu.ro.utcn.Market.Scheduler;

public enum SelectionPolicy {
    SHORTEST_QUEUE, SHORTEST_TIME
}
