package edu.ro.utcn.Market.Scheduler;

import edu.ro.utcn.Market.Cashier.CashRegister;
import edu.ro.utcn.Market.Cashier.Client;

import java.util.ArrayList;

public interface Strategy {
    public void addClient(ArrayList<CashRegister> cashRegisters, Client client);
}
