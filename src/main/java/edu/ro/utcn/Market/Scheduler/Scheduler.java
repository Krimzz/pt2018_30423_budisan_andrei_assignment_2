package edu.ro.utcn.Market.Scheduler;

import edu.ro.utcn.Market.Cashier.CashRegister;
import edu.ro.utcn.Market.Cashier.Client;
import edu.ro.utcn.Market.GUI.MainController;

import java.util.ArrayList;

//Sends Clients to CashRegisters according to the established strategy
public class Scheduler {
    private static ArrayList<CashRegister> cashRegisters;
    private Strategy strategy;

    private MainController gui;

    public Scheduler(int maxCashRegisters, int maxClientsPer, SelectionPolicy selectionPolicy, MainController gui) {
        //for maxCashRegisters
        //-create cashRegister object
        //-create thread with the object
        this.gui = gui;
        changeStrategy(selectionPolicy);
        cashRegisters = new ArrayList<>();
        for (int i = 0; i < maxCashRegisters; i++) {
            CashRegister cashRegister = new CashRegister(maxClientsPer, i + 1, gui);
            cashRegisters.add(cashRegister);
            Thread thread = new Thread(cashRegister);
            thread.setDaemon(true);
            thread.start();
        }
    }

    public void changeStrategy(SelectionPolicy policy) {
        //apply strategy pattern to instantiate the strategy with the concrete
        //strategy corresponding to policy
        if (policy == SelectionPolicy.SHORTEST_QUEUE) {
            strategy = new ConcreteStrategyQueue();
        }
        if (policy == SelectionPolicy.SHORTEST_TIME) {
            strategy = new ConcreteStrategyTime();
        }
    }

    public void dispatchClient(Client client) {
        //call strategy add task method
        strategy.addClient(cashRegisters, client);
    }

    public static void stopCashRegisters() {
        for (CashRegister cashRegister : cashRegisters) {
            cashRegister.setRunning(false);
        }
    }

    public void incrementEmptyQueuesTime() {
        for (CashRegister cashRegister : cashRegisters) {
            cashRegister.incrementEmptyQueueTime();
        }
    }

    public void getInfo() {
        int i = 1;
        for (CashRegister arr : cashRegisters) {
            gui.outLog("\n");
            gui.outLog("CashRegister " + i + "\n");
            gui.outLog("Average Wait Time: " + arr.getAvWaitTime() + "\n");
            gui.outLog("Average Service Time: " + arr.getAvProcessTime() + "\n");
            gui.outLog("Empty Queue Time: " + arr.getEmptyQTime() + "\n");

            System.out.println();
            System.out.println("CashRegister " + i);
            System.out.println("Average Wait Time: " + arr.getAvWaitTime());
            System.out.println("Average Service Time: " + arr.getAvProcessTime());
            System.out.println("Empty Queue Time: " + arr.getEmptyQTime());
            i++;
        }
    }

}
