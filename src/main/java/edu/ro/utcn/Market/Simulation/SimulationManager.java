package edu.ro.utcn.Market.Simulation;

import edu.ro.utcn.Market.Cashier.Client;
import edu.ro.utcn.Market.GUI.MainController;
import edu.ro.utcn.Market.Scheduler.Scheduler;
import edu.ro.utcn.Market.Scheduler.SelectionPolicy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class SimulationManager implements Runnable {
    //data read from UI
    private int timeLimit; //maximum processing time - from UI
    private int maxProcessingTime;
    private int minProcessingTime;
    private int maxArrivalTime;
    private int minArrivalTime;
    private int numberOfCashRegisters;
    private int numberOfClients;
    private int getNumberOfCashRegistersClients;

    public SelectionPolicy selectionPolicy;

    public MainController gui;

    //entity responsible with queue management and client distribution
    private Scheduler scheduler;
    //frame for displaying simulation
    //private SimulationFrame frame;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //pool of clients (client shopping in store)
    private ArrayList<Client> generatedClients;

    public SimulationManager(int timeLimit,int maxProcessingTime,int minProcessingTime,
                             int maxArrivalTime,int minArrivalTime,int numberOfCashRegisters,int numberOfClients,
                             int getNumberOfCashRegistersClients,SelectionPolicy selectionPolicy,MainController gui) {
        this.gui = gui;
        this.timeLimit = timeLimit;
        this.maxProcessingTime=maxProcessingTime;
        this.minProcessingTime = minProcessingTime;
        this.maxArrivalTime=maxArrivalTime;
        this.minArrivalTime=minArrivalTime;
        this.numberOfCashRegisters=numberOfCashRegisters;
        this.numberOfClients=numberOfClients;
        this.getNumberOfCashRegistersClients = getNumberOfCashRegistersClients;
        this.selectionPolicy = selectionPolicy;

        generatedClients = new ArrayList<>();

        //init the scheduler

        scheduler = new Scheduler(this.numberOfCashRegisters, this.getNumberOfCashRegistersClients, this.selectionPolicy,this.gui);

        // => create and start numberOfCashRegisters threads
        // => init selection strategy => createStrategy

        //init frame to display simulation !!!!!!!!!!!

        //generate numberOfClients clients using generateNRandomClients()
        //and store them to generatedClients

        generateNRandomClients();
    }

    private void generateNRandomClients() {

        //generate N random tasks;
        //-random processing time
        //minProcessingTime < processingTime < maxProcessingTime
        //-random arrivalTime
        //sort list with respect to arrivaltime

        for (int i = 0; i < numberOfClients; i++) {
            Random random = new Random();
            int arrivalTime = random.nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime;
            int processingTime = random.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime;
            Client client = new Client(i + 1, arrivalTime, processingTime);
            generatedClients.add(client);
        }

        Collections.sort(generatedClients, new Comparator<Client>() {
            public int compare(Client client1, Client client2) {
                return Integer.compare(client1.getArrivalTime().get(), client2.getArrivalTime().get());
            }
        });
    }

    @Override
    public void run() {
        int currentTime = 0;
        while (currentTime < timeLimit) {
            ArrayList<Client> removedClients = new ArrayList<>();

            //iterate generatedClients list and pick clients that have the
            //arrivalTime equal with the currentTime
            //-send client to queue by calling the dispatchClient method
            //from Scheduler
            //-delete client from list

            System.out.println("TIME: " + currentTime);
            gui.outLog("TIME: " + currentTime + "\n");

            for (Client client : generatedClients) {
                if (client.getArrivalTime().get() == currentTime) {
                    scheduler.dispatchClient(client);
                    removedClients.add(client);
                }
            }
            generatedClients.removeAll(removedClients);
            scheduler.incrementEmptyQueuesTime();

            //update UI frame !!!!!!!!!!!!!!!!!!
            //dai update in scheduler pt ca acolo ai lista de clients

            currentTime++;
            //wait an interval of 1 second
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        Scheduler.stopCashRegisters();
        scheduler.getInfo();
        gui.setLoading(false);
    }

    public Scheduler getScheduler(){
        return scheduler;
    }

}
